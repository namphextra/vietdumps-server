package controllers

import (
	beego "github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Ctx.Output.JSON("tada", true, true)
}
