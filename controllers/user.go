package controllers

import (
	"exam-web/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/cast"
	"net/http"
)

type UserController struct {
	beego.Controller
	UserService models.UserService
}

func (c *UserController) GetUser() {
	cookie := c.Ctx.Input.Cookie("X-Token")

	if cookie == "" {
		c.Ctx.Output.SetStatus(http.StatusUnauthorized)
		c.Ctx.Output.JSON(ResponseError{Success: false}, true, true)
		return
	}

	claims := &Claims{}

	tkn, err := jwt.ParseWithClaims(cookie, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(beego.AppConfig.String("jwtkey")), nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			c.Ctx.Output.SetStatus(http.StatusUnauthorized)
		} else {
			c.Ctx.Output.SetStatus(http.StatusBadRequest)
		}
		c.Ctx.Output.JSON(ResponseError{Success: false}, true, true)
		return
	}

	if !tkn.Valid {
		c.Ctx.Output.SetStatus(http.StatusUnauthorized)
		c.Ctx.Output.JSON(ResponseError{Success: false}, true, true)
		return
	}

	user, err := c.UserService.GetUser(models.UserRequest{Id: cast.ToInt64(c.Data["id"])})
	if err != nil {
		logs.Error("Can not get user, details: ", err.Error())
		c.Ctx.Output.SetStatus(http.StatusInternalServerError)
		c.Ctx.Output.JSON(ResponseError{Error: err.Error(), Success: false}, true, true)
		return
	}

	c.Ctx.Output.JSON(user, true, true)
}
