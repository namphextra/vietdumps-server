package controllers

type ResponseError struct {
	Error   interface{} `json:"error"`
	Success bool        `json:"success"`
}
