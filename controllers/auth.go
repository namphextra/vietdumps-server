package controllers

import (
	"encoding/json"
	"exam-web/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"time"
)

type AuthController struct {
	beego.Controller
	UserService models.UserService
}

type SignUpBody struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Claims struct {
	Email string
	jwt.StandardClaims
}

func (c *AuthController) SignUp() {
	input := c.Ctx.Input
	var data SignUpBody
	err := json.Unmarshal(input.RequestBody, &data)
	if err != nil {
		logs.Error(err)
		c.Ctx.Output.SetStatus(http.StatusInternalServerError)
		c.Ctx.Output.JSON(ResponseError{Error: err.Error(), Success: false}, true, true)
		return
	}

	user, err := c.UserService.GetUser(models.UserRequest{Email: data.Email})
	if err != nil {
		logs.Error(err)
		c.Ctx.Output.SetStatus(http.StatusInternalServerError)
		c.Ctx.Output.JSON(ResponseError{Error: err.Error(), Success: false}, true, true)
		return
	}

	if user.Id > 0 {
		c.Ctx.Output.SetStatus(http.StatusBadRequest)
		c.Ctx.Output.JSON(ResponseError{Error: "User existed", Success: false}, true, true)
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)
	if err != nil {
		logs.Error(err)
		c.Ctx.Output.SetStatus(http.StatusInternalServerError)
		c.Ctx.Output.JSON(ResponseError{Error: err.Error(), Success: false}, true, true)
		return
	}

	_, err = c.UserService.CreateUser(models.UserModal{Email: data.Email, Password: string(hashedPassword), CreatedAt: time.Now().Unix(), UpdatedAt: time.Now().Unix()})
	if err != nil {
		c.Ctx.Output.SetStatus(http.StatusInternalServerError)
		c.Ctx.Output.JSON(ResponseError{Success: false, Error: err.Error()}, true, true)
	}
	c.Ctx.Output.JSON(map[string]bool{"success": true}, true, true)
}

func (c *AuthController) SignIn() {
	input := c.Ctx.Input
	var data SignUpBody
	err := json.Unmarshal(input.RequestBody, &data)

	if err != nil {
		logs.Error(err)
		c.Ctx.Output.SetStatus(http.StatusInternalServerError)
		c.Ctx.Output.JSON(ResponseError{Error: err.Error(), Success: false}, true, true)
		return
	}

	user, err := c.UserService.GetUser(models.UserRequest{Email: data.Email})
	if err != nil {
		logs.Error(err)
		c.Ctx.Output.SetStatus(http.StatusInternalServerError)
		c.Ctx.Output.JSON(ResponseError{Error: err.Error(), Success: false}, true, true)
		return
	}
	expireToken := time.Now().Add(7 * 24 * time.Hour).Unix()
	claims := &Claims{
		Email: user.Email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireToken,
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStrings, err := token.SignedString([]byte(beego.AppConfig.String("jwtkey")))

	if err != nil {
		logs.Error("Can not generate token, details: ", err)
		c.Ctx.Output.SetStatus(http.StatusInternalServerError)
		c.Ctx.Output.JSON(ResponseError{Error: err.Error(), Success: false}, true, true)
		return
	}

	c.Ctx.SetCookie("X-Token", tokenStrings, "expires", expireToken)
	c.Ctx.Output.JSON(map[string]bool{"success": true}, true, true)
}
