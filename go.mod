module exam-web

go 1.15

require github.com/astaxie/beego v1.12.1

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/shiena/ansicolor v0.0.0-20200904210342-c7312218db18 // indirect
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/cast v1.3.1
	golang.org/x/crypto v0.0.0-20201124201722-c8d3bf9c5392
)
