package models

import (
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
)

type UserModal struct {
	Id        int64  `json:"id"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	UpdatedAt int64  `json:"updated_at"`
	CreatedAt int64  `json:"created_at"`
}

func (u *UserModal) TableName() string {
	return "user"
}

type UserService struct {
}

type UserRequest struct {
	Id    int64
	Email string
}

func (m *UserService) GetUser(req UserRequest) (*UserModal, error) {
	o := orm.NewOrm()
	user := new(UserModal)
	qs := o.QueryTable(user)

	if req.Email != "" {
		qs = qs.Filter("email", req.Email)
	}
	if req.Id > 0 {
		qs = qs.Filter("id", req.Id)
	}

	err := qs.One(user)
	if err != nil {
		logs.Error(err)
		return nil, err
	}
	return user, nil
}

func (m *UserService) CreateUser(user UserModal) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(&user)
	if err != nil {
		logs.Error(err)
		return 0, err
	}
	return id, nil
}
