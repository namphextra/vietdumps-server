package main

import (
	"exam-web/models"
	_ "exam-web/routers"
	beego "github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

func init()  {
	orm.RegisterModel(new(models.UserModal))
	err := orm.RegisterDataBase("default", "mysql", "root:root@/vietdumps?charset=utf8", 30)
	if err != nil {
		logs.Error(err)
	}
}

func main() {
	orm.Debug = true
	beego.BConfig.WebConfig.Session.SessionOn = true
	beego.Run()
}

