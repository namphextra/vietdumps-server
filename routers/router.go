package routers

import (
	"exam-web/controllers"
	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSRouter("/", &controllers.MainController{}, "get:Get"),
		beego.NSRouter("/sign-up", &controllers.AuthController{}, "post:SignUp"),
		beego.NSRouter("/sign-in", &controllers.AuthController{}, "post:SignIn"),
		beego.NSRouter("/user", &controllers.UserController{}, "get:GetUser"),
	)
	beego.AddNamespace(ns)
}
